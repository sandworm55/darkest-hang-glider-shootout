/* global basicFunction, GenFunctions, jstestdriver */

basicFunction = TestCase( "basicFunction" );

basicFunction.prototype.setUp = function ()
{
    jstestdriver.console.log( "testing basic functions" );
};

basicFunction.prototype.testPercent1 = function ()
{
    assertEquals( 20 , GenFunctions.getPercentage( 2 , 10 ) );
};

basicFunction.prototype.testPercent2 = function ()
{
    assertEquals( 4 , GenFunctions.getPercentage( 4 , 91 ) );
};

basicFunction.prototype.testPercent3 = function ()
{
    assertEquals( 20 , GenFunctions.getPercentage( 2 , 10 ) );
};

basicFunction.prototype.testPercent4 = function ()
{
    assertEquals( "1" , GenFunctions.getPercentage( 199 , 10284 , true ) );
};

basicFunction.prototype.testPercent5 = function ()
{
    assertNotEquals( 1 , GenFunctions.getPercentage( 199 , 1000 ) );
};

basicFunction.prototype.tearDown = function ()
{
    jstestdriver.console.log( "done testing basic functions" );
};