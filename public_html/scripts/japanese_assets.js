/* global glider */

glider.japanese_controller = ( function ()
{

    var screenCanvas = glider.screens["canvas"];

    var dom = glider.com ,
	    canvasJapanseAssets = document.getElementById( "entities" );

    // guard tower
    var guardtower = new Graphic( glider.assets.japanese_assets.GuardTower , canvasJapanseAssets );
    var gt = new Obstacle( screenCanvas.image , guardtower , new vector( 300 , 490 ) , 0 );

    // house 1
    var house1 = new Graphic( glider.assets.japanese_assets.House1 , canvasJapanseAssets );
    var hs1 = new DynamicSprite( screenCanvas.image , house1 , new vector( 500 , 540 ) , 0 );

    function japanSpeedUpdate()
    {
	gt.setVelocity( new vector( Math.floor( -1 * glider.screens["canvas"].speed / 2 ) , 0 ) );
	hs1.setVelocity( new vector( Math.floor(  -1 * glider.screens["canvas"].speed / 2 ) , 0 ) );

	gt.setPos( new vector( gt.getPos( ).x , glider.screens["canvas"].screenHeight + ( canvasJapanseAssets.height - gt.halfHeight( ) ) ) );
	hs1.setPos( new vector( hs1.getPos( ).x , glider.screens["canvas"].screenHeight + ( canvasJapanseAssets.height - hs1.halfHeight( ) ) ) );
    }

    function japanese_assets()
    {
	for ( var item in screenCanvas.entList )
	{
	    if ( screenCanvas.entList[item].getType() === "GuardTower" )
	    {
		if ( gt.getPos().x < 0 - gt.halfWidth() )
		{
		    gt.move( new vector( Math.floor( Math.random() * ( 2000 ) ) + 3000 , 0 ) );
		    gt.setVelocity( new vector( Math.floor( -1 * glider.screens["canvas"].speed / 2 ) , 0 ) );

		}
	    }
	} // end for guard tower


	for ( var item in screenCanvas.entList )
	{
	    if ( screenCanvas.entList[item].getType() === "House1" )
	    {
		if ( hs1.getPos().x < 0 - hs1.halfWidth() )
		{
		    hs1.move( new vector( Math.floor( Math.random() * ( 2000 ) ) + 1000 , 0 ) );
		    hs1.setVelocity( new vector( Math.floor( -1 * glider.screens["canvas"].speed / 2 ) , 0 ) );

		}
	    }
	}

    }

    function push_japanese_assets()
    {
	screenCanvas.entList.push( gt );
	screenCanvas.entList.push( hs1 );
    }
    return{
	japanese_assets : japanese_assets ,
	push_japanese_assets : push_japanese_assets,
	japanSpeedUpdate :japanSpeedUpdate
    };

} )();


