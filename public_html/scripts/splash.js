/* global glider */

glider.screens["splash-screen"] = ( function ()
{
    var firstRun = true;

    function setup()
    {
	var dom = glider.dom ,
		$ = dom.$ ,
		screen = $( "#splash-screen" )[0];

	$( ".continue" )[0].style.display = "block";

	dom.bind( screen , "click" , function ()
	{
	    glider.showScreen( "main-menu" );
	} );

	var buttons = $( "button" );

	for ( var i = 0; i < buttons.length; i++ )
	{

	    buttons[i].addEventListener( "mousedown" , playDownSound );
	    buttons[i].addEventListener( "mouseup" , playUpSound );
	}

    }

    function playDownSound()
    {
	glider.assets.audio.sfx.typeWriterClickDown_sfx.play();

    }

    function playUpSound()
    {
	glider.assets.audio.sfx.typeWriterClickUp_sfx.play();

    }
    function checkProgress()
    {
	var $ = glider.dom.$ ,
		p = glider.getLoadProgress() * 100;
	$( "#splash-screen .indicator" )[0].style.width = p + "%";

	if ( p === 100 )
	    setup();
	else
	    setTimeout( checkProgress , 30 );
    }

    function run()
    {
	if ( firstRun )
	{
	    checkProgress();
	    firstRun = false;
	}
    }

    return {
	run : run
    };
} )();