/* global glider */

glider.russian_controller = ( function ()
{


    var screenCanvas = glider.screens["canvas"];

    var dom = glider.com ,
	    canvasRussianAssets = document.getElementById( "entities" );

    // ship
    var ship = new Graphic( glider.assets.russian_assets.Ship , canvasRussianAssets );
    var sh = new DynamicSprite( screenCanvas.image , ship , new vector( 2000 , 550 ) , 0 );

    function russianSpeedUpdate()
    {
	sh.setVelocity( new vector( Math.floor( -1 * glider.screens["canvas"].speed / 2 ) , 0 ) );

	sh.setPos( new vector( sh.getPos( ).x , glider.screens["canvas"].screenHeight + ( canvasRussianAssets.height - sh.halfHeight( ) ) ) );
    }


    function russian_assets()
    {
	for ( var item in screenCanvas.entList )
	{
	    if ( screenCanvas.entList[item].getType() === "Ship" )
	    {
		if ( sh.getPos().x < 0 - sh.halfWidth() )
		{
		    sh.move( new vector( Math.floor( Math.random() * ( 2000 ) ) + 1000 , 0 ) );
		    sh.setVelocity( new vector( Math.floor( -1 * glider.screens["canvas"].speed / 2 ) , 0 ) );
		}
	    }
	}
    }

    function push_russian_assets()
    {
	screenCanvas.entList.push( sh );
    }

    return{
	russian_assets : russian_assets ,
	push_russian_assets : push_russian_assets ,
	russianSpeedUpdate : russianSpeedUpdate
    };


} )();
