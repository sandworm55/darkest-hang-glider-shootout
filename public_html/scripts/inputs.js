/* global glider */

glider.input = ( function ()
{
    var inputHandlers ,
	    up = false ,
	    down = false;

    function initialize()
    {

	var dom = glider.dom ,
		$ = dom.$ ,
		controls = glider.settings.controls ,
		background = $( "#background" )[0] ,
		forground = $( "#forground" )[0] ,
		entities = $( "#entities" )[0] ,
		ui = $( "#ui" )[0] ;

	inputHandlers = { };

	dom.bind( ui , "mousedown" , function ( event )
	{
	    handleClick( event , "CLICK" , event );
	} );
	dom.bind( ui , "touchstart" , function ( event )
	{
	    handleClick( event , "TOUCH" , event.targetTouches[0] );
	} );

	document.addEventListener( "keydown" , function ( event )
	{
	    if ( event.keyCode === 87 )
	    {
		if ( !down )
		    up = true;
	    }
	    else if ( event.keyCode === 83 )
	    {
		if ( !up )
		    down = true;
	    }
	    else if ( event.keyCode === 38 )
	    {
		if ( !down )
		    up = true;
	    }
	    else if ( event.keyCode === 40 )
	    {
		if ( !up )
		    down = true;
	    }
	    // console.log("KeyDown ---------- inputs.js\nUp: " + up + "\nDown: " + down);
	} );

	document.addEventListener( "keyup" , function ( event )
	{
	    if ( event.keyCode === 87 )
	    {
		up = false;
	    }
	    else if ( event.keyCode === 83 )
	    {
		down = false;
	    }
	    else if ( event.keyCode === 38 )
	    {
		up = false;
	    }
	    else if ( event.keyCode === 40 )
	    {
		down = false;
	    }
	    // console.log("KeyUp ---------- inputs.js\nUp: " + up + "\nDown: " + down);
	} );

    }

    function bind( action , handler )
    {
	if ( !inputHandlers[action] )
	    inputHandlers[action] = [ ];
	inputHandlers[action].push( handler );
    }


    function handleClick( event , control , click )
    {
	var settings = glider.settings ,
		action = settings.controls[control];

	if ( !action )
	    return;

	var background = glider.dom.$( "#background" )[0] ,
		rect = background.getBoundingClientRect() ,
		relX , relY ,
		jewelX , jewelY;

	relX = click.clientX - rect.left;
	relY = click.clientY - rect.top;

	console.log( "x: " + relX + " y: " + relY );

	event.preventDefault();
    }

    function getUp()
    {
	return up;
    }

    function getDown()
    {
	return down;
    }

    return {
	initialize : initialize ,
	bind : bind ,
	getUp : getUp ,
	getDown : getDown
    };
} )();