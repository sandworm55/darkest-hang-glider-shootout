/* global glider */

glider.controller = ( function ()
{
    var screenCanvas = glider.screens["canvas"];
    var dom = glider.dom ,
	    canvasAssets = document.getElementById( "entities" );

    // var speed = 10;

    // console.log( "enemySpawned" );

    // enemies
    var enemy1 = new Graphic( glider.assets.shared_assets.Enemy1 , canvasAssets );
    var e1 = new Glider( screenCanvas.image , enemy1 , new vector( -100 , 0 ) , 0 );

    var enemy2 = new Graphic( glider.assets.shared_assets.Enemy2 , canvasAssets );
    var e2 = new Glider( screenCanvas.image , enemy2 , new vector( -100 , 0 ) , 0 );

    screenCanvas.entList.push( e1 );
    screenCanvas.entList.push( e2 );

    // heat vents

    var heatvent = new Graphic( glider.assets.shared_assets.HeatVent , canvasAssets );
    var hv = new DynamicSprite( screenCanvas.image , heatvent , new vector( 1000 , 530 ) , 0 );

    screenCanvas.entList.push( hv );

    function speedUpdate()
    {
	e1.setVelocity( new vector( Math.floor( -5 + -1 * glider.screens["canvas"].speed / 2 ) , 0 ) );
	e2.setVelocity( new vector( Math.floor( -7 + -1 * glider.screens["canvas"].speed / 2 ) , 0 ) );

	hv.setVelocity( new vector( Math.floor( -1 * glider.screens["canvas"].speed / 2 ) , 0 ) );

	hv.setPos( new vector( hv.getPos( ).x , glider.screens["canvas"].screenHeight + ( canvasAssets.height - hv.halfHeight( ) ) ) );
	e1.setPos( new vector( e1.getPos( ).x , e1SpawnY + glider.screens["canvas"].screenHeight ) );
	e2.setPos( new vector( e2.getPos( ).x , e2SpawnY + glider.screens["canvas"].screenHeight ) );
    }

    function spawn_vents()
    {
	for ( var item in screenCanvas.entList )
	{
	    if ( screenCanvas.entList[item].getType() === "HeatVent" )
	    {
		if ( hv.getPos().x < -hv.halfWidth() )
		{
		    hv.move( new vector( Math.floor( Math.random() * ( 2000 * glider.screens["canvas"].ventSpawnTalentMod ) ) + 1000 , 0 ) );
		    hv.setVelocity( new vector( Math.floor( -1 * glider.screens["canvas"].speed / 2 ) , 0 ) );

		}
	    }
	}
    }

    var e1SpawnY ,
	    e2SpawnY;

    function respawn_enemy()
    {

	for ( var item in screenCanvas.entList )
	{
	    if ( screenCanvas.entList[item].getType() === "Enemy1" )
	    {

		if ( e1.getPos().x < 0 - e1.halfWidth() )
		{
		    e1SpawnY = Math.floor( ( Math.random() * 300 ) + 0 );
		    e1.setPos( new vector( Math.floor( Math.random() * ( 1000 ) ) + 1000 , e1SpawnY ) );
		}
	    }
	}
	for ( var item in screenCanvas.entList )
	{
	    if ( screenCanvas.entList[item].getType() === "Enemy2" )
	    {
		if ( e2.getPos().x < 0 - e2.halfWidth() )
		{
		    e2SpawnY = Math.floor( ( Math.random() * 300 ) + 0 );
		    e2.setPos( new vector( Math.floor( Math.random() * ( 1000 ) ) + 1000 , e2SpawnY ) );
		}
	    }
	}
    }

    function repush_asset()
    {
	screenCanvas.entList.push( e1 );
	screenCanvas.entList.push( e2 );
	screenCanvas.entList.push( hv );
    }
    return{
	respawn_enemy : respawn_enemy ,
	spawn_vents : spawn_vents ,
	repush_assets : repush_asset ,
	speedUpdate : speedUpdate
    };
} )();


