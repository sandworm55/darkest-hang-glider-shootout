/* global glider */
glider.assets = ( function ()
{

    var images = {
	japan_sprite_sheet : new Image() ,
	canada_sprite_sheet : new Image() ,
	russian_sprite_sheet : new Image() ,
	test_sprite_sheet : new Image() ,
	mission1_button : new Image() ,
	mission2_button : new Image() ,
	mission3_button : new Image()
    };

    var audio = {
	bgm : {
	    russian_bgm : new Audio() ,
	    japan_bgm : new Audio() ,
	    canada_bgm : new Audio() ,
	    menu_bgm : new Audio()
	} ,
	sfx : {
	    crash_sfx : new Audio() ,
	    goose_sfx : new Audio() ,
	    typeWriterClickDown_sfx : new Audio() ,
	    typeWriterClickUp_sfx : new Audio() ,
	    typeWriterPage_sfx : new Audio() ,
	    fail_sfx : new Audio()
	}
    };

    for ( var item in audio.bgm )
    {
	audio.bgm[item].loop = true;
	audio.bgm[item].volume = glider.settings.musicVal;
    }

    for ( var item in audio.sfx )
    {
	audio.sfx[item].volume = glider.settings.soundVal;
    }

    var shared_assets = {

	Player : {
	    Name : "Player" ,
	    height : 128 ,
	    width : 128 ,
	    X : 0 ,
	    Y : 0
	} ,

	Enemy1 : {
	    Name : "Enemy1" ,
	    height : 128 ,
	    width : 128 ,
	    X : 128 ,
	    Y : 0
	} ,

	Enemy2 : {
	    Name : "Enemy2" ,
	    height : 128 ,
	    width : 128 ,
	    X : 256 ,
	    Y : 0
	} ,

	HeatVent : {
	    Name : "HeatVent" ,
	    height : 256 ,
	    width : 128 ,
	    X : 1152 ,
	    Y : 128

	} ,

	BackGround : {
	    Name : "Background" ,
	    height : 366 ,
	    width : 512 ,
	    X : 0 ,
	    Y : 512

	} ,

	Foreground : {
	    Name : "Foreground" ,
	    height : 128 ,
	    width : 512 ,
	    X : 0 ,
	    Y : 384

	}
    };

    var japanese_assets = {

	GuardTower : {
	    Name : "GuardTower" ,
	    height : 256 ,
	    width : 256 ,
	    X : 384 ,
	    Y : 0

	} ,

	Arrow : {
	    Name : "Arrow" ,

	    height : 29 ,
	    width : 65 ,
	    X : 671 ,
	    Y : 50

	} ,

	NinjaStar : {
	    Name : "NinjaStar" ,
	    height : 41 ,
	    width : 40 ,
	    X : 812 ,
	    Y : 44

	} ,

	Hill2 : {
	    Name : "Hill2" ,
	    height : 128 ,
	    width : 512 ,
	    X : 512 ,
	    Y : 384

	} ,

	House1 : {
	    Name : "House1" ,
	    height : 128 ,
	    width : 128 ,
	    X : 512 ,
	    Y : 640

	} ,

	House2 : {
	    Name : "House2" ,
	    height : 128 ,
	    width : 128 ,
	    X : 640 ,
	    Y : 640

	} ,

	House3 : {
	    Name : "House3" ,
	    height : 128 ,
	    width : 128 ,
	    X : 768 ,
	    Y : 640

	} ,

	House4 : {
	    Name : "House4" ,
	    height : 128 ,
	    width : 128 ,
	    X : 896 ,
	    Y : 640

	} ,

	LongHouse : {
	    Name : "LongHouse" ,
	    height : 256 ,
	    width : 128 ,
	    X : 1024 ,
	    Y : 640

	} ,

	cherry_tree1 : {
	    Name : "cherry_tree1" ,
	    height : 110 ,
	    width : 128 ,
	    X : 1024 ,
	    Y : 768

	} ,

	cherry_tree2 : {
	    Name : "cherry_tree2" ,
	    height : 128 ,
	    width : 110 ,
	    X : 1152 ,
	    Y : 768

	} ,

	japanese_heat_vent1 : {
	    Name : "j_heat_vent1" ,
	    hegiht : 256 ,
	    width : 127 ,
	    X : 767 ,
	    Y : 128
	}
    };

    var russian_assets = {

	Ship : {

	    Name : "Ship" ,
	    height : 128 ,
	    width : 256 ,
	    X : 384 ,
	    Y : 0
	} ,

	OilDrill : {

	    Name : "OilDrill" ,
	    height : 256 ,
	    width : 256 ,
	    X : 640 ,
	    Y : 0
	} ,

	Missle : {

	    Name : "Missle" ,
	    height : 36 ,
	    width : 44 ,
	    X : 878 ,
	    Y : 110
	} ,

	Filter : {

	    Name : "Fitler" ,
	    height : 365 ,
	    width : 512 ,
	    X : 1024 ,
	    Y : 64
	} ,

	russian_heat_vent1 : {
	    Name : "r_heat_vent" ,
	    height : 256 ,
	    width : 256 ,
	    X : 640 ,
	    Y : 256
	} ,

	russian_heat_vent2 : {
	    Name : "r_heat_vent2" ,
	    height : 256 ,
	    width : 256 ,
	    X : 896 ,
	    Y : 256
	}

    };


    var canadian_assets = {

	Mountian1 : {

	    Name : "Mountian1" ,
	    height : 256 ,
	    width : 256 ,
	    X : 384 ,
	    Y : 0
	} ,

	Mountian2 : {

	    Name : "Mountian2" ,
	    height : 256 ,
	    width : 256 ,
	    X : 640 ,
	    Y : 0
	} ,

	Laser : {

	    Name : "Laser" ,
	    height : 14 ,
	    width : 149 ,
	    X : 949 ,
	    Y : 64
	} ,

	canadian_heat_vent1 : {
	    Name : "c_heat_vent1" ,
	    hegiht : 256 ,
	    width : 128 ,
	    X : 896 ,
	    Y : 256
	} ,

	canadian_heat_vent2 : {
	    Name : "c_heat_vent1" ,
	    hegiht : 256 ,
	    width : 128 ,
	    X : 1024 ,
	    Y : 256
	} ,

	canadian_heat_vent3 : {
	    Name : "c_heat_vent3" ,
	    hegiht : 256 ,
	    width : 128 ,
	    X : 1152 ,
	    Y : 256
	}
    };
    return {
	shared_assets : shared_assets ,
	canadian_assets : canadian_assets ,
	russian_assets : russian_assets ,
	japanese_assets : japanese_assets ,
	images : images ,
	audio : audio

    };
} )();