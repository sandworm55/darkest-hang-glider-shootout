/* global glider */

glider.screens["main-menu"] = ( function ()
{
    var dom = glider.dom ,
	    firstRun = true;

    function setup()
    {
	dom.bind( "#main-menu ul.menu" , "click" , function ( e )
	{
	    if ( e.target.nodeName.toLowerCase() === "button" )
	    {
		var action = e.target.getAttribute( "name" );
		glider.showScreen( action );
	    }
	} );
    }

    function run()
    {
	glider.assets.audio.bgm.menu_bgm.play();
	if ( firstRun )
	{
	    setup();
	    firstRun = false;
	}
    }

    return {
	run : run
    };
} )();