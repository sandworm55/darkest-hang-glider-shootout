/* global glider */

glider.screens["about-screen"] = ( function ()
{
    var dom = glider.dom ,
	    firstRun = true;

    function setup()
    {
	dom.bind( "#about-screen" , "click" , function ( e )
	{
	    if ( e.target.nodeName.toLowerCase() === "button" )
	    {
		var action = e.target.getAttribute( "name" );
		if ( action === "main-menu" )
		    glider.showScreen( action );
	    }
	} );
    }

    function run()
    {
	if ( firstRun )
	{
	    setup();
	    firstRun = false;
	}
    }

    return {
	run : run
    };
} )();