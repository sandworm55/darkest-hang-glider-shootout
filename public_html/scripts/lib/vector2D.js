class vector
{
    constructor( x , y )
    {
	this.x = x;
	this.y = y;
    }

    set( x , y )
    {
	this.x = x;
	this.y = y;
    }

    add( vector2 )
    {
	return new vector( this.x + vector2.x , this.y + vector2.y );
    }

    subtract( vector2 )
    {
	return new vector( this.x - vector2.x , this.y - vector2.y );
    }

    multiply( scale )
    {
	return new vector( this.x * scale , this.y * scale );
    }

    magnitude()
    {
	return Math.sqrt( this.x * this.x + this.y * this.y );
    }

    normalize( vector2 )
    {
	return new vector( this.x / this.magnitude( vector2 ) , this.y / this.magnitude( vector2 ) );
    }

    speedTo( scale )
    {
	return this.normalize().multiply( scale );
    }

    rotation()
    {
	return ( Math.atan2( this.y , this.x ) ) * 180 / Math.PI;
	// (* 180 / Math.PI) to be replaced with to toDegree function
    }

    dotproduct( vector2 )
    {
	return this.x * vector2.x + this.y * vector2.y;
    }

    projection( vector2 )
    {
	return this.multiply( this.dotproduct( vector2 ) / ( this.magnitude() * this.magnitude() ) );
    }

////
    print()
    {
	return "X : " + this.x + "   Y : " + this.y;
    }
}
