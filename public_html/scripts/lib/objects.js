class Graphic
{
    constructor( asset_object , canvas )
    {
	this.canvas = canvas;
	this.name = asset_object.Name;
	this.srcPosVector = new vector( asset_object.X , asset_object.Y );
	this.srcSizeVector = new vector( asset_object.width , asset_object.height );
	this.sizeVector = new vector( asset_object.width , asset_object.height );
    }
}


class DynamicSprite
{
    constructor( img , graphic , posVector , r )
    {
	this.img = img;
	this.graphic = graphic;
	this.posVector = posVector;
	this.velocity = new vector( 0 , 0 );
	this.r = r;
    }

    getType( )
    {
	return this.graphic.name;
    }

    halfWidth( )
    {
	return ( this.graphic.sizeVector.x / 2 );
    }

    halfHeight( )
    {
	return ( this.graphic.sizeVector.y / 2 );
    }

    center( )
    {
	var x = this.posVector.x + this.halfWidth( ) ,
		y = this.posVector.y + this.halfHeight( );
	return { x , y };
    }

    setVelocity( vector )
    {
	this.velocity = vector;
    }

    changeVelocity( vector )
    {
	this.velocity = this.velocity.add( vector );
    }

    moveSelf( )
    {
	this.move( this.velocity );
    }

    draw( )
    {
	var ctx = this.graphic.canvas.getContext( "2d" );
	if ( this.r === 0 )
	{
	    ctx.drawImage( this.img ,
		    this.graphic.srcPosVector.x , this.graphic.srcPosVector.y ,
		    this.graphic.srcSizeVector.x , this.graphic.srcSizeVector.y ,
		    this.posVector.x - this.halfWidth( ) , this.posVector.y - this.halfHeight( ) ,
		    this.graphic.sizeVector.x , this.graphic.sizeVector.y );
	}
	else
	{
	    ctx.save( );
	    ctx.translate( this.posVector.x , this.posVector.y );
	    ctx.rotate( this.r * Math.PI / 180 );
	    ctx.translate( -this.halfWidth( ) , -this.halfHeight( ) );
	    ctx.drawImage( this.img ,
		    this.graphic.srcPosVector.x , this.graphic.srcPosVector.y ,
		    this.graphic.srcSizeVector.x , this.graphic.srcSizeVector.y ,
		    0 , 0 ,
		    this.graphic.sizeVector.x , this.graphic.sizeVector.y );
	    ctx.restore( );
	}
    }

    collisionCheck( that )
    {
	if ( hitTestRectangle( this , that ) )
	{
	    return true;
	}
	else
	{
	    return false;
	}

    }

    move( vector )
    {
	this.posVector = this.posVector.add( vector );
    }

    rotate( r )
    {
	this.r += r;
    }

    setPos( vector )
    {
	this.posVector = vector;
    }

    getPos( )
    {
	return this.posVector;
    }

    printPos( )
    {
	console.log( this.posVector.print( ) );
    }
}

class Obstacle extends DynamicSprite
{
    constructor( img , graphic , posVector )
    {
	super( img , graphic , posVector );
    }
}

class Glider extends DynamicSprite
{
    constructor( img , graphic , posVector )
    {
	super( img , graphic , posVector );
    }
}

class Player extends DynamicSprite
{
    constructor( img , graphic , posVector , r )
    {
	super( img , graphic , posVector , r );
    }
}


class MessageObject
{
    constructor( text , vector , canvas )
    {
	this.pos = vector;
	this.text = text;
	this.canvas = canvas;

	this.visible = true;
	this.font = "normal bold 20px Helvetica";
	this.fontStyle = "White";
	this.textBaseline = "top";
    }

    draw()
    {
	var ctx = this.canvas.getContext( "2d" );
	ctx.fillStyle = this.fontStyle;
	ctx.font = this.font;

	ctx.fillText( this.text , this.pos.x , this.pos.y );
    }
}

var spriteID = function ( id , rotation )
{
    if ( id < 0 || id > 7 )
	console.log( "Invalid ID : " + id );
    if ( rotation < 0 || rotation > 3 )
	console.log( "Invalid Rotation : " + rotation );
    this.id = id;
    this.rotation = rotation;
};
//var simpleRect = function ( x , y , w , h )
//{
//    this.x = x;
//    this.y = y;
//    this.h = h;
//    this.w = w;
//    this.left = function ()
//    {
//	return this.x;
//    };
//    this.right = function ()
//    {
//	return this.x + this.w;
//    };
//    this.top = function ()
//    {
//	return this.y;
//    };
//    this.bottom = function ()
//    {
//	return this.y + this.h;
//    };
//    this.halfWidth = function ()
//    {
//	return this.w / 2;
//    };
//    this.halfHeight = function ()
//    {
//	return this.h / 2;
//    };
//    this.center = function ()
//    {
//	return {
//	    x : this.x + ( this.halfWidth ) ,
//	    y : this.y + ( this.halfHeight )
//	};
//    };
//};
