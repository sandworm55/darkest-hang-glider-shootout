/* global glider */

glider.screens["talents-screen"] = ( function ()
{
    var dom = glider.dom ,
	    firstRun = true ,
	    talents = glider.talents ,
	    maxValuesPerTalent = 2;

    function setup()
    {
	dom.bind( "#talents-screen" , "click" , function ( e )
	{
	    if ( e.target.nodeName.toLowerCase() === "button" )
	    {
		var action = e.target.getAttribute( "name" );
		if ( action === "main-menu" )
		    glider.showScreen( action );
		else if ( action === "talents-screen-endurance" )
		    glider.showScreen( action );
		//else if ( action === "talents-screen-special" )
		    //glider.showScreen( action );
		else if ( action === "talents-screen-flight" )
		    glider.showScreen( action );
		else if ( action === "reset" )
		    glider.resetTalents();
	    }
	} );
    }

    function run()
    {
	if ( firstRun )
	{
	    setup();
	    firstRun = false;
	}
	dom.addClass( dom.$( "#talents-screen-flight" )[0] , "active" );
    }

    return {
	run : run
    };
} )();

glider.screens["talents-screen-flight"] = ( function ()
{
    var dom = glider.dom ,
	    firstRun = true;

    function setup()
    {
	/*dom.bind( "#flight" , "click" , function ( e )
	 {
	 if ( e.target.nodeName.toLowerCase() === "button" )
	 {
	 var action = e.target.getAttribute( "name" );
	 if ( action === "back" )
	 glider.showScreen( action );
	 else if ( action === "endurance" )
	 glider.showScreen( action );
	 else if ( action === "special" )
	 glider.showScreen( action );
	 
	 }
	 } );*/
    }

    function run()
    {
	if ( firstRun )
	{
	    setup();
	    firstRun = false;
	}
    }

    return {
	run : run
    };
} )();

glider.screens["talents-screen-endurance"] = ( function ()
{
    var dom = glider.dom ,
	    firstRun = true;

    function setup()
    {
	/*dom.bind( "#flight" , "click" , function ( e )
	 {
	 if ( e.target.nodeName.toLowerCase() === "button" )
	 {
	 var action = e.target.getAttribute( "name" );
	 if ( action === "back" )
	 glider.showScreen( action );
	 else if ( action === "endurance" )
	 glider.showScreen( action );
	 else if ( action === "special" )
	 glider.showScreen( action );
	 
	 }
	 } );*/
    }

    function run()
    {
	if ( firstRun )
	{
	    setup();
	    firstRun = false;
	}
    }

    return {
	run : run
    };
} )();

/*glider.screens["talents-screen-special"] = ( function ()
{
    var dom = glider.dom ,
	    firstRun = true;

    function setup()
    {
	/*dom.bind( "#flight" , "click" , function ( e )
	 {
	 if ( e.target.nodeName.toLowerCase() === "button" )
	 {
	 var action = e.target.getAttribute( "name" );
	 if ( action === "back" )
	 glider.showScreen( action );
	 else if ( action === "endurance" )
	 glider.showScreen( action );
	 else if ( action === "special" )
	 glider.showScreen( action );
	 
	 }
	 } );
    }

    function run()
    {
	if ( firstRun )
	{
	    setup();
	    firstRun = false;
	}
    }

    return {
	run : run
    };
} )();*/