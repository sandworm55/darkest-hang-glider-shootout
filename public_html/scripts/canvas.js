/* global glider, Player, Glider, Obstacle */


glider.screens["canvas"] = ( function ( )
{
    var dom = glider.dom ,
	    entList = [ ] ,
	    firstRun = true ,
	    image = new Image( ) ,
	    bgm , playCrash = true , playFail = true ,
	    canvasBg = document.getElementById( "background" ) ,
	    canvasE = document.getElementById( "entities" ) ,
	    canvasFg = document.getElementById( "forground" ) ,
	    canvasUi = document.getElementById( "ui" ) ,
	    fg , fg2 , fg3 , character , score , speedMsg , structure , alt , running , drawingTimer;
    var canvasList = [ document.getElementById( "background" ) ,
	document.getElementById( "forground" ) ,
	document.getElementById( "entities" ) ,
	document.getElementById( "ui" ) ];

    //physics
    var speed = 10 ,
	    fall = 1 ,
	    screenHeight = 0 ,
	    heatUp = false ,
	    heatUpNum = 0 ,
	    testMode = false; //////// debug mode

    var japanese_level = false;
    var russian_level = false;
    var canadian_level = false;
    var topSpeed1 = false ,
	    topSpeed2 = false ,
	    topSpeed3 = false ,
	    topSpeedMod = 0 ,
	    topSpeedArray = [ ] ,
	    speedLimit = 60 ,
	    riseTalentMod = 1 ,
	    diveTalentMod = 1 ,
	    dragTalentMod = 1 ,
	    ventTalentMod = 1 ,
	    rotateUpTalentMod = 1 ,
	    rotateDownTalentMod = 1 ,
	    rotateFastTalentMod = 1 ,
	    rotateSlowTalentMod = 1 ,
	    ventSpawnTalentMod = 1;

    function restart( )
    {
	run( );
    }

    function setup( )
    {
	var input = glider.input;
	input.initialize( );
	canvasResize( );
	dom.bind( "#canvas" , "click" , function ( e )
	{
	    if ( e.target.nodeName.toLowerCase( ) === "button" )
	    {
		var action = e.target.getAttribute( "name" );
		glider.screens["pause-menu"].run( );
		dom.addClass( dom.$( "#" + action )[0] , "active" );
		glider.screens["canvas"].running = false;
	    }
	} );
    }

    function run( )
    {
	clearInterval( drawingTimer );
	entList.length = 0;
	speed = 10;
	fall = 1;
	screenHeight = 0;

	glider.controller.repush_assets();
	glider.japanese_controller.push_japanese_assets();
	glider.russian_controller.push_russian_assets();
	glider.canadian_controller.push_canadian_assets();
	glider.screens["canvas"].running = true;

	if ( firstRun )
	{
	    setup( );
	    firstRun = false;
	}

	japanese_level = false;
	russian_level = false;
	canadian_level = false;
	checkTalents( );
	switch ( glider.stage )
	{
	    case "1":
		image.src = "images/Japan_sprite_sheet.png";
		glider.japanese_controller.push_japanese_assets();
		japanese_level = true;
		glider.screens["canvas"].bgm = glider.assets.audio.bgm.japan_bgm;
		break;
	    case "2":
		image.src = "images/Russian_sprite_sheet.png";
		glider.russian_controller.push_russian_assets();
		russian_level = true;
		glider.screens["canvas"].bgm = glider.assets.audio.bgm.russian_bgm;
		break;
	    case "3":
		image.src = "images/Canada_sprite_sheet.png";
		glider.canadian_controller.push_canadian_assets();
		canadian_level = true;
		glider.screens["canvas"].bgm = glider.assets.audio.bgm.canada_bgm;
		break;
	    default:
		console.log( "No Stage Selected" );
		break;
	}

	//image.src = "images/Spire_sheet_canvas.png";

	spriteSetup();

	msgSetup();

	entList.push( score );
	entList.push( alt );
	entList.push( speedMsg );
	//entList.push( structure );
	glider.screens["canvas"].bgm.play();
	drawing( );
    }

    function spriteSetup()
    {
	var bgGraphic = new Graphic( glider.assets.shared_assets.BackGround , canvasBg );
	bgGraphic.sizeVector = new vector( 800 , 600 );
	var bg = new DynamicSprite( image , bgGraphic , new vector( 400 , 300 ) , 0 );
	entList.push( bg );

	var fgGraphic = new Graphic( glider.assets.shared_assets.Foreground , canvasFg );
	fg = new DynamicSprite( image , fgGraphic , new vector( 256 , canvasFg.height - glider.assets.shared_assets.Foreground.height ) , 0 );
	entList.push( fg );

	fg2 = new DynamicSprite( image , fgGraphic , new vector( 256 + ( 2 * 256 ) , canvasFg.height - glider.assets.shared_assets.Foreground.height ) , 0 );
	entList.push( fg2 );

	fg3 = new DynamicSprite( image , fgGraphic , new vector( 256 + ( 4 * 256 ) , canvasFg.height - glider.assets.shared_assets.Foreground.height ) , 0 );
	entList.push( fg3 );

	var charGlider = new Graphic( glider.assets.shared_assets.Player , canvasE );
	character = new Player( image , charGlider , new vector( 64 , 64 ) , 0 , 0 , 0 );
	entList.push( character );

	glider.screens["canvas"].character = character;
    }

    function msgSetup()
    {
	score = {
	    value : 0 ,
	    labelMsg : new MessageObject( "Score: " , new vector( 640 , 28 ) , canvasUi ) ,
	    valueMsg : new MessageObject( this.value , new vector( 720 , 28 ) , canvasUi ) ,
	    draw : function ( )
	    {
		this.labelMsg.draw( );
		this.valueMsg.draw( );
	    } ,
	    getType : function ( )
	    {
		return "messege";
	    } ,
	    moveSelf : function ( )
	    {
		this.value += glider.screens["canvas"].speed;
		this.valueMsg.text = Math.floor( this.value );
	    }
	};
	speedMsg = {
	    value : 0 ,
	    labelMsg : new MessageObject( "Speed: " , new vector( 640 , 540 ) , canvasUi ) ,
	    valueMsg : new MessageObject( this.value , new vector( 720 , 540 ) , canvasUi ) ,
	    draw : function ( )
	    {
		this.labelMsg.draw( );
		this.valueMsg.draw( );
	    } ,
	    getType : function ( )
	    {
		return "messege";
	    } ,
	    moveSelf : function ( )
	    {
		this.value = Math.floor( glider.screens["canvas"].speed );
		this.valueMsg.text = this.value;
	    }
	};
	alt = {
	    value : 0 ,
	    labelMsg : new MessageObject( "Altitude: " , new vector( 640 , 570 ) , canvasUi ) ,
	    valueMsg : new MessageObject( this.value , new vector( 720 , 570 ) , canvasUi ) ,
	    draw : function ( )
	    {
		this.labelMsg.draw( );
		this.valueMsg.draw( );
	    } ,
	    getType : function ( )
	    {
		return "messege";
	    } ,
	    moveSelf : function ( )
	    {
		this.value = 568 - Math.floor( glider.screens["canvas"].character.getPos( ).y );
		if ( this.value < 0 )
		    this.value = 0;
		this.valueMsg.text = this.value;
	    }
	};
	structure = {
	    value : 0 ,
	    labelMsg : new MessageObject( "Structure: " , new vector( 20 , 28 ) , canvasUi ) ,
	    valueRect : 100 ,
	    bgRect : [ 101 , 21 , 98 , 18 ] ,
	    draw : function ( )
	    {
		this.labelMsg.draw( );
		var ctx = canvasUi.getContext( "2d" );
		ctx.strokeStyle = ( "white" );
		ctx.lineWidth = "4";
		ctx.strokeRect( 119 , 9 , 102 , 22 );
		ctx.fillStyle = ( "green" );
		ctx.fillRect( 120 , 10 , this.valueRect , 20 );
	    } ,
	    getType : function ( )
	    {
		return "messege";
	    } ,
	    moveSelf : function ( )
	    {
		//set to a % of structure,
		//change color from green to red as it lowers
	    }
	};
    }

    function checkTalents( )
    {
	speedLimit = 60;
	topSpeedArray.length = 0;
	topSpeedMod = 0;
	riseTalentMod = 1;
	diveTalentMod = 1;
	dragTalentMod = 1;
	ventTalentMod = 1;
	rotateUpTalentMod = 1;
	rotateDownTalentMod = 1;
	rotateFastTalentMod = 1;
	rotateSlowTalentMod = 1;
	ventSpawnTalentMod = 1;


	// Modifiers from talent tree.
	topSpeed1 = document.getElementById( "endurance1a" ).checked;
	if ( topSpeed1 )
	    console.log( "topSpeed1 is: " + topSpeed1 );
	topSpeed2 = document.getElementById( "endurance2a" ).checked;
	topSpeed3 = document.getElementById( "endurance3a" ).checked;

	topSpeedArray.push( topSpeed1 );
	topSpeedArray.push( topSpeed2 );
	topSpeedArray.push( topSpeed3 );

	for ( var i = 0; i < topSpeedArray.length; i++ )
	{
	    if ( topSpeedArray[i] === true )
		topSpeedMod++;
	}


	//speed limit
	speedLimit = speedLimit + ( 5 * topSpeedMod );

	// Rise dive, and drag Endurance Talents
	var divingTalentBool = false ,
		risingTalentBool = false ,
		dragTalentBool = false;

	risingTalentBool = document.getElementById( "endurance1b" ).checked;
	divingTalentBool = document.getElementById( "endurance2b" ).checked;
	dragTalentBool = document.getElementById( "endurance3b" ).checked;

	if ( risingTalentBool === true )
	    riseTalentMod = 0.9;
	if ( divingTalentBool === true )
	    diveTalentMod = 1.1;
	if ( dragTalentBool === true )
	    dragTalentMod = 0.9;

	//vent Flight Talent
	var ventTalentBool = false;

	ventTalentBool = document.getElementById( "flight3a" ).checked;

	if ( ventTalentBool === true )
	    ventTalentMod = 1.2;

	var ventSpawnTalentBool = document.getElementById( "flight3b" );

	if ( ventSpawnTalentBool === true )
	    ventSpawnTalentMod = 0.8;
    }

    function canvasResize( )
    {
	var dp = document.getElementById( "canvas" );
	var rect = dp.getBoundingClientRect( );
	for ( var i = 0; i < canvasList.length; i++ )
	{
	    canvasList[i].width = rect.width;
	    canvasList[i].height = rect.height;
	}
    }

    function drawing( )
    {
	drawingTimer = setInterval( function ( )
	{
	    if ( glider.screens["canvas"].running )
	    {
		glider.screens["canvas"].speed = speed;
		glider.screens["canvas"].screenHeight = screenHeight;
		glider.controller.respawn_enemy( );
		glider.controller.spawn_vents( );
		glider.controller.speedUpdate( );
		glider.japanese_controller.japanSpeedUpdate();
		glider.russian_controller.russianSpeedUpdate();
		moveLevel( );
		moveCharacter( );

		if ( japanese_level === true )
		    glider.japanese_controller.japanese_assets( );
		else if ( russian_level === true )
		    glider.russian_controller.russian_assets( );
		else if ( canadian_level === true )
		    glider.canadian_controller.candian_assets( );
		for ( var i = 1; i < canvasList.length; i++ )
		{
		    var c = ( canvasList[i] );
		    var ctx = c.getContext( "2d" );
		    ctx.clearRect( 0 , 0 , canvasList[i].width , canvasList[i].height );
		}

		for ( var i = 0; i < entList.length; i++ )
		{
		    entList[i].moveSelf( );
		}

		collisions( );
		for ( var i = 0; i < entList.length; i++ )
		{
		    entList[i].draw( );
		}
	    }
	} , 33 );
    }

    function collisions( )
    {
	for ( var item in entList )
	{

	    if ( entList[item] instanceof Glider )
	    {
		if ( entList[item].collisionCheck( character ) )
		{
		    speed -= 0.3;
		    tryPlayCrash();
		}
	    }

	    if ( entList[item] instanceof Obstacle )
	    {
		if ( entList[item].collisionCheck( character ) )
		{
		    speed = 0;
		    tryPlayCrash();
		}
	    }

	    else if ( entList[item].getType( ) === "HeatVent" )
	    {
		if ( entList[item].collisionCheck( character ) )
		{
		    heatUp = true;
		}
		else
		{
		    heatUp = false;
		}
	    }

	}
    }

    function tryPlayCrash()
    {
	if ( glider.screens["canvas"].playCrash === true )
	{
	    glider.assets.audio.sfx.crash_sfx.pause();
	    glider.assets.audio.sfx.crash_sfx.currentTime = 0;
	    glider.assets.audio.sfx.crash_sfx.play();
	    glider.screens["canvas"].playCrash = false;
	    setTimeout( function ()
	    {
		glider.screens["canvas"].playCrash = true;
	    } , 500 );
	}
    }

    function moveLevel( )
    {
	if ( speed >= speedLimit )
	    speed = speedLimit;
	fg.setVelocity( new vector( Math.floor( -1 * speed / 2 ) , 0 ) );
	fg2.setVelocity( new vector( Math.floor( -1 * speed / 2 ) , 0 ) );
	fg3.setVelocity( new vector( Math.floor( -1 * speed / 2 ) , 0 ) );

	fg.setPos( new vector( fg.getPos( ).x , screenHeight + ( canvasFg.height - fg.halfHeight( ) ) ) );
	fg2.setPos( new vector( fg2.getPos( ).x , screenHeight + ( canvasFg.height - fg2.halfHeight( ) ) ) );
	fg3.setPos( new vector( fg3.getPos( ).x , screenHeight + ( canvasFg.height - fg2.halfHeight( ) ) ) );

	//land location reset 1
	if ( fg.getPos( ).x < 0 - fg.halfWidth( ) )
	{
	    fg.setPos( new vector( fg3.getPos( ).x + fg3.halfWidth( ) * 2 , screenHeight + ( canvasFg.height - fg.halfHeight( ) ) ) );
	}

	//land location reset 2
	if ( fg2.getPos( ).x < 0 - fg2.halfWidth( ) )
	{
	    fg2.setPos( new vector( fg.getPos( ).x + fg.halfWidth( ) * 2 , screenHeight + ( canvasFg.height - fg2.halfHeight( ) ) ) );
	}

	//land location reset 3
	if ( fg3.getPos( ).x < 0 - fg3.halfWidth( ) )
	{
	    fg3.setPos( new vector( fg2.getPos( ).x + fg2.halfWidth( ) * 2 , screenHeight + ( canvasFg.height - fg3.halfHeight( ) ) ) );
	}
    }

    function moveCharacter( )
    {

	var rotateUpTalentBool = document.getElementById( "flight1b" ) ,
		rotateDownTalentBool = document.getElementById( "flight1a" ) ,
		rotateFastTalentBool = document.getElementById( "flight2a" ) ,
		rotateSlowTalentBool = document.getElementById( "flight2b" );

	var hypot = 1 / Math.acos( Math.sign( ( character.r * Math.PI / 180 ) / 2 ) * ( character.r * Math.PI / 180 ) / 2 );
	hypot *= 3;

	var heightChange = hypot * Math.sin( character.r * Math.PI / 180 );
	heightChange *= 3;

	var rotation = 1.5;
	rotation *= 3;

	var accel = ( Math.sign( character.r ) / 100 );
	accel *= 10;

	// Increased turning rate talents check

	if ( rotateUpTalentBool === true )
	    if ( character.r < 0 )
		rotateUpTalentMod = 1.2;
	if ( rotateDownTalentBool === true )
	    if ( character.r > 0 )
		rotateDownTalentMod = 1.2;
	if ( rotateFastTalentBool === true )
	    if ( speed > speedLimit / 2 )
		rotateFastTalentMod = 1.2;
	if ( rotateSlowTalentBool === true )
	    if ( speed < speedLimit / 2 )
		rotateSlowTalentMod = 1.2;

	if ( rotateFastTalentBool === true || rotateSlowTalentBool === true )
	    rotation = rotation * rotateSlowTalentMod * rotateFastTalentMod;


	//gravity
	if ( character.r < 0 )
	{
	    fall = fall - character.r / 900;
	}
	else
	{
	    fall = 1;
	}
	if ( testMode )
	    fall = 0;

	//out top of screen
	if ( character.getPos( ).y - 64 <= 0 )
	{
	    if ( heightChange < 0 )
	    {
		screenHeight += -( fall + heightChange );
		character.setVelocity( new vector( 0 , 0 ) );
	    }
	    else if ( heightChange > 0 )
	    {
		screenHeight += -( fall + heightChange );
		character.setVelocity( new vector( 0 , 0 ) );
	    }
	}

	//on screen
	if ( screenHeight <= 0 )
	{
	    screenHeight = 0;
	    character.setVelocity( new vector( 0 , fall + heightChange ) );
	}

	//if above bottom of canvas
	if ( character.getPos( ).y + character.halfWidth( ) / 2 < canvasE.height )
	{
	    //slow down or speed up based on nose angle
	    if ( character.r < 0 )
	    {
		speed += accel * riseTalentMod / 10;
	    }
	    else if ( character.r > 0 )
	    {
		speed += accel * diveTalentMod;
	    }

	    //no reversing
	    if ( speed <= 0 )
	    {
		speed = 0;
	    }

	    //constant speed for test mode
	    if ( testMode )
		speed = 30;

	    //pressing up
	    if ( glider.input.getUp( ) && speed > 0 )
	    {
		if ( character.r > -70 )
		{
		    character.rotate( -rotation * rotateUpTalentMod );
		}
	    }

	    //pressing down
	    if ( glider.input.getDown( ) )
	    {
		if ( character.r < 80 )
		{
		    character.rotate( rotation * rotateDownTalentMod );
		}
	    }

	    if ( heatUp )
	    {
		heatUpNum = -15 * ventTalentMod;
	    }
	    else if ( heatUpNum < 0 )
	    {
		heatUpNum += 5;
	    }
	    else
	    {
		heatUpNum = 0;
	    }

	    character.move( new vector( 0 , heatUpNum ) );

	    // character.changeVelocity(new vector(0,heatUpNum));




	}

	//if hit bottom of canvas
	if ( character.getPos( ).y + character.halfWidth( ) / 2 >= canvasE.height )
	{
	    //sudden movement drop
	    character.setVelocity( new vector( 0 , 0 ) );
	    if ( !testMode )
	    {
		glider.assets.audio.sfx.fail_sfx.pause();
		glider.assets.audio.sfx.fail_sfx.currentTime = 0;
		glider.assets.audio.sfx.fail_sfx.play();
		//reduce speed
		speed -= speed / 10;
		//full stop
		if ( speed < 0.001 )
		{
		    speed = 0;

		    //loser here
		    restart();

		}
	    }
	}


	//console.log( "Speed : \t\t\t" + speed + "\nRotation : \t\t\t" + character.r + "\nVertical Speed : \t" + heightChange );
    }
    return {
	run : run ,
	speed : speed ,
	image : image ,
	entList : entList ,
	character : character ,
	running : running ,
	japanese_level : japanese_level ,
	ventSpawnTalentMod : ventSpawnTalentMod ,
	screenHeight : screenHeight ,
	bgm : bgm ,
	playCrash : playCrash ,
	playFail : playFail

    };
} )( );



