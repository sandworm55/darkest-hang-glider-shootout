/* global glider */

glider.screens["options-menu"] = ( function ()
{
    var dom = glider.dom ,
	    $ = dom.$ ,
	    firstRun = true;
    function setup()
    {
	dom.bind( "#options-menu" , "click" , function ( e )
	{
	    if ( e.target.nodeName.toLowerCase() === "button" )
	    {
		var action = e.target.getAttribute( "name" );
		if ( action === "main-menu" )
		    glider.showScreen( action );
	    }
	    else if ( e.target.nodeName.toLowerCase() === "input" )
	    {
		if ( e.target.getAttribute( "type" ) === "checkbox" )
		{
		    if ( e.target.getAttribute( "name" ) === "musicBool" )
			glider.settings.musicBool = !e.target.checked;
		    else if ( e.target.getAttribute( "name" ) === "soundBool" )
			glider.settings.soundBool = !e.target.checked;
		}
		else if ( e.target.getAttribute( "type" ) === "range" )
		{
		    if ( e.target.getAttribute( "name" ) === "musicVal" )
			glider.settings.musicVal = e.target.value;
		    else if ( e.target.getAttribute( "name" ) === "soundVal" )
			glider.settings.soundVal = e.target.value;
		}

		//for ( var i = 0; i < glider.assets.audio.bgm; i++ )
		for ( var item in glider.assets.audio.bgm )
		{
		    glider.assets.audio.bgm[item].volume = glider.settings.musicVal;
		    glider.assets.audio.bgm[item].muted = glider.settings.musicBool;	    
		}

		for ( var item in glider.assets.audio.sfx )
		{
		    glider.assets.audio.sfx[item].volume = glider.settings.soundVal;
		    glider.assets.audio.sfx[item].muted = glider.settings.soundBool;
		}
		window.localStorage.musicVal = glider.settings.musicVal;
		window.localStorage.musicBool = glider.settings.musicBool;
		window.localStorage.soundVal = glider.settings.soundVal;
		window.localStorage.soundBool = glider.settings.soundBool;
	    }
	} );
    }

    function run()
    {
	if ( firstRun )
	{
	    setup();
	    firstRun = false;
	}
	//$("music_slide")
	//$("#music_check").checked = glider.settings.musicBool;
	//$("sfx_slide")
	//$("#sfx_check").checked = glider.settings.soundBool;
    }

    return {
	run : run
    };
} )();