/* global glider */

glider.screens["pause-menu"] = ( function ()
{
    var dom = glider.dom ,
	    $ = dom.$ ,
	    firstRun = true;
	    
    function setup()
    {
	dom.bind( "#pause-menu" , "click" , function ( e )
	{
	    if ( e.target.nodeName.toLowerCase() === "button" )
	    {
		var action = e.target.getAttribute( "name" );
		if ( action === "canvas" )
		{
		    dom.removeClass($( "#pause-menu" )[0], "active" );
		    glider.screens["canvas"].running = true;
		    console.log( glider.screens["canvas"].running , glider.screens["canvas"].running = true );
		}
		else if (action === "main-menu")
		{
		    dom.removeClass($( "#pause-menu" )[0], "active" );
		    glider.showScreen( "game-screen" );
		    glider.screens["canvas"].bgm.pause();
		    glider.screens["canvas"].bgm.currentTime = 0;
		    glider.assets.audio.bgm.menu_bgm.play();
		}
	    }
	} );
    }

    function run()
    {
	if ( firstRun )
	{
	    setup();
	    firstRun = false;
	}
    }

    return {
	run : run
    };
} )();