
function flightToEndurance()
{
    document.getElementById( "flight" ).style.display = "none";
    document.getElementById( "endurance" ).style.display = "block";
}

function flightToSpecial()
{
    document.getElementById( "flight" ).style.display = "none";
    document.getElementById( "special" ).style.display = "block";
}

function enduranceToFlight()
{
    document.getElementById( "endurance" ).style.display = "none";
    document.getElementById( "flight" ).style.display = "block";
}

function enduranceToSpecial()
{
    document.getElementById( "endurance" ).style.display = "none";
    document.getElementById( "special" ).style.display = "block";
}

function specialToFlight()
{
    document.getElementById( "special" ).style.display = "none";
    document.getElementById( "flight" ).style.display = "block";
}

function specialToEndurance()
{
    document.getElementById( "special" ).style.display = "none";
    document.getElementById( "endurance" ).style.display = "block";
}
