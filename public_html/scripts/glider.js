var glider = ( function ()
{
    var stage;
    var settings = {
	soundBool : false ,
	musicBool : false ,
	soundVal : .5 ,
	musicVal : .5 ,
//run a function on start up
//checks if there are vvalues
//if so, reset the values to those
	checkValues : function ()
	{
	    if ( window.localStorage.length !== 0 )
	    {
		console.log("welcome back");
		glider.settings.soundBool = (window.localStorage.soundBool==="true"?true:false);
		glider.settings.musicBool = (window.localStorage.musicBool==="true"?true:false);
		glider.settings.soundVal = window.localStorage.soundVal;
		glider.settings.musicVal = window.localStorage.musicVal;
		
		for ( var item in glider.assets.audio.sfx )
		{
		    glider.assets.audio.sfx[item].volume = glider.settings.soundVal;
		    glider.assets.audio.sfx[item].muted = glider.settings.soundBool;
		}
		for ( var item in glider.assets.audio.bgm )
		{
		    glider.assets.audio.bgm[item].volume = glider.settings.musicVal;
		    glider.assets.audio.bgm[item].muted = glider.settings.musicBool;
		}


		glider.dom.$("#music_slide")[0].value = glider.settings.musicVal;
		glider.dom.$("#sfx_slide")[0].value = glider.settings.soundVal;
		glider.dom.$("#music_check")[0].checked = (glider.settings.musicBool?false:true);
		glider.dom.$("#sfx_check")[0].checked = (glider.settings.soundBool?false:true);
	    }
	} ,

	clearSettings : function ()
	{
	    window.localStorage.removeItem( "soundBool" );
	    window.localStorage.removeItem( "musicBool" );
	    window.localStorage.removeItem( "soundVal" );
	    window.localStorage.removeItem( "musicVal" );

	    soundBool = false;
	    musicBool = false;
	    soundVal = .5;
	    musicVal = .5;
	} ,

	controls : {
	    CLICK : "startMouseSteer" ,
	    TOUCH : "startMouseSteer" ,
	    W : "up" ,
	    UP : "up" ,
	    S : "down" ,
	    DOWN : "down"
	} };

    var scriptQueue = [ ] ,
	    assetQueue = [ ] ,
	    numResourcesLoaded = 0 ,
	    numResources = 0 ,
	    executeRunning = false ,
	    assetRunning = false;

    function getLoadProgress()
    {
	console.log( numResourcesLoaded + "/" + numResources );
	return numResourcesLoaded / numResources;
    }

    function executeScriptQueue()
    {
	var next = scriptQueue[0] ,
		first , script;

	if ( next && next.loaded )
	{
	    executeRunning = true;
	    scriptQueue.shift();

	    first = document.getElementsByTagName( "script" )[0];
	    script = document.createElement( "script" );
	    script.onload = function ()
	    {
		if ( next.callback )
		    next.callback();
		executeScriptQueue();
	    };
	    script.src = next.src;
	    first.parentNode.insertBefore( script , first );
	}
	else
	{
	    executeRunning = false;
	}
    }

    function executeAssetQueue()
    {
	var next = assetQueue[0];

	if ( next && next.loaded )
	{
	    assetRunning = true;
	    assetQueue.shift();
	}
	else
	{
	    assetRunning = false;
	}
    }

    function load( src , object , callback )
    {
	var image , queueEntry;
	numResources++;

	// add this resource to the executing queue
	queueEntry = {
	    src : src ,
	    callback : callback ,
	    loaded : false
	};



	if ( object === "script" )
	{
	    scriptQueue.push( queueEntry );
	    //console.log("first:adding " + src + " to queue");
	    image = new Image();
	    image.onerror = function ()
	    {
		numResourcesLoaded++;
		queueEntry.loaded = true;
		//console.log("loaded " + src);
		if ( !executeRunning )
		{
		    executeScriptQueue();
		}

	    };
	    image.src = src;
	}
	else
	{
	    assetQueue.push( queueEntry );
	    //console.log("second:adding " + src + " to queue");
	    object.onload = onLoadAsset( src );
	    object.src = src;
	}

    }

    function onLoadAsset( src )
    {
	numResourcesLoaded++;
	assetQueue.loaded = true;
	if ( !assetRunning )
	{
	    executeAssetQueue();
	}
    }

    function setup()
    {
	console.log( "Success!" );
	settings.checkValues();
	glider.showScreen( "splash-screen" );
    }

    function showScreen( screenID )
    {
	glider.assets.audio.sfx.typeWriterPage_sfx.play();
	var dom = glider.dom ,
		$ = dom.$ ,
		activeScreen = $( "#game .screen.active" )[0] ,
		screen = $( "#" + screenID )[0];

	if ( !glider.screens[screenID] )
	{
	    alert( "This module is not implemented yet." );
	    return;
	}

	if ( activeScreen )
	{
	    if ( activeScreen.getAttribute( "id" ) === "talents-screen" &&
		    screen.getAttribute( "id" ) !== "main-menu" )
	    {
		activeScreen = $( "#game .screen .talents.active" )[0];
		dom.removeClass( activeScreen , "active" );
		glider.screens[screenID].run();
	    }
	    else if ( activeScreen.getAttribute( "id" ) === "talents-screen" &&
		    screen.getAttribute( "id" ) === "main-menu" )
	    {
		activeScreen = $( "#game .screen .talents.active" )[0];
		dom.removeClass( activeScreen , "active" );
		activeScreen = $( "#game .screen.active" )[0];
		dom.removeClass( activeScreen , "active" );
	    }
	    else
		dom.removeClass( activeScreen , "active" );
	}
	dom.addClass( screen , "active" );
	glider.screens[screenID].run();
    }
    
    function resetTalents() {
	var eleF1 = document.getElementsByName("flight1");
	var eleF2 = document.getElementsByName("flight2");
	var eleF3 = document.getElementsByName("flight3");
	var eleE1 = document.getElementsByName("endurance1");
	var eleE2 = document.getElementsByName("endurance2");
	var eleE3 = document.getElementsByName("endurance3");
	
	for ( var i = 0; i < 2; i++ )
	{
	    eleF1[i].checked = false;
	    eleF2[i].checked = false;
	    eleF3[i].checked = false;
	    eleE1[i].checked = false;
	    eleE2[i].checked = false;
	    eleE3[i].checked = false;
	}
    }

    return {
	load : load ,
	setup : setup ,
	showScreen : showScreen ,
	screens : { } ,
	settings : settings ,
	getLoadProgress : getLoadProgress ,
	stage : stage,
	resetTalents : resetTalents
    };
} )();