/* global glider */
glider.talents = ( function ()
{
    var dom = glider.dom ,
	    $ = dom.$;

    var talents =
	    {
		Flight :
			{
			    name : "Flight" ,
			    tier1 :
				    {
					flightA :
						{
						    enabled : false ,
						    value : 0 ,
						    element : $( "#flight1a" )[0]
						} ,
					flightB :
						{
						    enabled : false ,
						    value : 0 ,
						    element : $( "#flight1b" )[0]
						}
				    } ,
			    tier2 :
				    {
					flightA :
						{
						    enabled : false ,
						    value : 0 ,
						    element : $( "#flight2a" )[0]
						} ,
					flightB :
						{
						    enabled : false ,
						    value : 0 ,
						    element : $( "#flight2b" )[0]
						}
				    } ,
			    tier3 :
				    {
					flightA :
						{
						    enabled : false ,
						    value : 0
						} ,
					flightB :
						{
						    enabled : false ,
						    value : 0
						}
				    } ,
			    tier4 :
				    {
					flightA :
						{
						    enabled : false ,
						    value : 0
						} ,
					flightB :
						{
						    enabled : false ,
						    value : 0
						}
				    } ,
			    tier5 :
				    {
					flightA :
						{
						    enabled : false ,
						    value : 0
						} ,
					flightB :
						{
						    enabled : false ,
						    value : 0
						}
				    }
			} ,
		Endurance :
			{
			    name : "Endurance" ,
			    tier1 :
				    {
					enduranceA :
						{
						    enabled : false ,
						    value : 0
						} ,
					enduranceB :
						{
						    enabled : false ,
						    value : 0
						}
				    } ,
			    tier2 :
				    {
					enduranceA :
						{
						    enabled : false ,
						    value : 0
						} ,
					enduranceB :
						{
						    enabled : false ,
						    value : 0
						}
				    } ,
			    tier3 :
				    {
					enduranceA :
						{
						    enabled : false ,
						    value : 0
						} ,
					endurance :
						{
						    enabled : false ,
						    value : 0
						}
				    } ,
			    tier4 :
				    {
					enduranceA :
						{
						    enabled : false ,
						    value : 0
						} ,
					enduranceB :
						{
						    enabled : false ,
						    value : 0
						}
				    } ,
			    tier5 :
				    {
					enduranceA :
						{
						    enabled : false ,
						    value : 0
						} ,
					enduranceB :
						{
						    enabled : false ,
						    value : 0
						}
				    }
			} ,

		Special :
			{
			    name : "Special" ,
			    tier1 :
				    {
					specialA :
						{
						    enabled : false ,
						    value : 0
						} ,
					specialB :
						{
						    enabled : false ,
						    value : 0
						}
				    } ,
			    tier2 :
				    {
					specialA :
						{
						    enabled : false ,
						    value : 0
						} ,
					specialB :
						{
						    enabled : false ,
						    value : 0
						}
				    } ,
			    tier3 :
				    {
					specialA :
						{
						    enabled : false ,
						    value : 0
						} ,
					specialB :
						{
						    enabled : false ,
						    value : 0
						}
				    } ,
			    tier4 :
				    {
					specialA :
						{
						    enabled : false ,
						    value : 0
						} ,
					specialB :
						{
						    enabled : false ,
						    value : 0
						}
				    } ,
			    tier5 :
				    {
					specialA :
						{
						    enabled : false ,
						    value : 0
						} ,
					specialB :
						{
						    enabled : false ,
						    value : 0
						}
				    }
			}
	    };

    return {
	talents : talents
    };
} )();
