/* global glider */

glider.screens["game-screen"] = ( function ()
{
    var dom = glider.dom ,
	    firstRun = true;

    function setup()
    {
	dom.bind( "#game-screen" , "click" , function ( e )
	{
	    if ( e.target.nodeName.toLowerCase() === "img" )
	    {
		glider.stage = ( e.target.getAttribute( "name" ) );
		glider.showScreen( 'canvas' );
		glider.assets.audio.bgm.menu_bgm.pause();
		glider.assets.audio.bgm.menu_bgm.currentTime = 0;
	    }
	    else if ( e.target.nodeName.toLowerCase() === "button" )
	    {
		var action = e.target.getAttribute( "name" );
		if ( action === "main-menu" )
		    glider.showScreen( action );
	    }
	} );

    }

    function run()
    {
	if ( firstRun )
	{
	    setup();
	    firstRun = false;
	}
    }

    return {
	run : run
    };
} )();