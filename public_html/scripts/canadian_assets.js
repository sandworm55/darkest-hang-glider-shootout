/*global glider*/

glider.canadian_controller = ( function ()
{


    var screenCanvas = glider.screens["canvas"];

    var dom = glider.com ,
	    canvasCanadianAssets = document.getElementById( "entities" );

    // mountian

    var mountian = new Graphic( glider.assets.canadian_assets.Mountian1 , canvasCanadianAssets );
    var mnt1 = new Obstacle( screenCanvas.image , mountian , new vector( 2000 , 535 ) , 0 );

    function canadianSpeedUpdate()
    {
	mnt1.setVelocity( new vector( Math.floor( -1 * glider.screens["canvas"].speed / 2 ) , 0 ) );

	mnt1.setPos( new vector( mnt1.getPos( ).x , glider.screens["canvas"].screenHeight + ( canvasCanadianAssets.height - mnt1.halfHeight( ) ) ) );
    }

    function candian_assets()
    {
	for ( var item in screenCanvas.entList )
	{
	    if ( screenCanvas.entList[item].getType() === "Mountian1" )
	    {
		if ( mnt1.getPos().x < 0 - mnt1.halfWidth() )
		{
		    mnt1.move( new vector( Math.floor( Math.random() * ( 2000 ) ) + 1000 , 0 ) );
		    mnt1.setVelocity( new vector( Math.floor( -1 * glider.screens["canvas"].speed / 2 ) , 0 ) );

		}
	    }
	} // mountian 
    }

    function push_canadian_assets()
    {
	screenCanvas.entList.push( mnt1 );
    }
    return {
	candian_assets : candian_assets ,
	push_canadian_assets : push_canadian_assets,
	canadianSpeedUpdate : canadianSpeedUpdate
    };

} )();


